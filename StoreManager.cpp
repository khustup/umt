/*
 * StoreManager.cpp
 *
 *  Created on: Jul 5, 2015
 *      Author: gevorg
 */

#include "StoreManager.h"
#include <iostream>
#include "MyUtils.h"
//#include <string>
#include <boost/lexical_cast.hpp>

namespace umt {

extern const std::string WHITESPACES = " \t-:";

StoreManager::StoreManager(sql::Connection* con, int user_id) : m_userid(user_id), m_con(con)
{
}

StoreManager::~StoreManager()
{
}



bool StoreManager::fncAdd(std::string str)
{
	std::cout << "StoreManager::fncAdd >> " << str << std::endl;
	try{
		std::vector<std::string> args;
		MyUtils::split(str, umt::WHITESPACES, args);
		int store_id = 1;
		std::string data;
		if (args.size() < 1)
			return false;

		if (args.size() >= 2)
		{
			store_id = boost::lexical_cast<int>(args[0]);
			data = args[1];
		}
		else
		{ // store id not given by user, only data is given
			sql::Statement *stmt = m_con->createStatement();
			sql::ResultSet* res = stmt->executeQuery("SELECT MAX(store_id) + 1 FROM umtdb.store WHERE user_id = "
													+ MyUtils::to_string(m_userid) + ";");
			if (res->next())
			{
				store_id = res->getInt(1);
			}
			data = args[0];
		}

		std::string sql_cmd = "INSERT INTO umtdb.store(user_id, store_id, store_string) VALUES( ? , ? , ?);";
		sql::PreparedStatement *prep_stmt = m_con->prepareStatement(sql_cmd);
		prep_stmt->setInt(1, m_userid);
		//prep_stmt->setInt(2, std::stoi(args[0]));
		prep_stmt->setInt(2, store_id);  //
		prep_stmt->setString(3, data);
		prep_stmt->execute();
		delete prep_stmt;
	}
	catch (...) {
		return false;
	}

	return true;
}

bool StoreManager::fncRemove(std::string str)
{
	std::cout << "StoreManager::fncRemove >> " << str << std::endl;
	try{
		std::vector<std::string> args;
		MyUtils::split(str, umt::WHITESPACES, args);

		if (args.size() < 1)
			return false;

		std::string sql_cmd = "DELETE FROM umtdb.store WHERE user_id = ? AND store_id = ? ;";
		sql::PreparedStatement *prep_stmt = m_con->prepareStatement(sql_cmd);
		prep_stmt->setInt(1, m_userid);
		//prep_stmt->setInt(2, std::stoi(args[0]));
		prep_stmt->setInt(2, boost::lexical_cast<int>(args[0]));

		prep_stmt->execute();
		delete prep_stmt;
	}
	catch (...) {
		return false;
	}
	return true;
}

bool StoreManager::fncList(std::string str)
{
	std::cout << "StoreManager::fncList >> " << str << std::endl;
	sql::Statement *stmt = m_con->createStatement();
	std::string sel_cmd = "SELECT store_id, store_string FROM umtdb.store WHERE user_id = " + MyUtils::to_string(m_userid) + ";";
	sql::ResultSet* res = stmt->executeQuery(sel_cmd);
	while (res->next())
	{
		std::cout << res->getInt(1) << "\t" << res->getString(2) << std::endl;
	}
	delete res;
	delete stmt;

	return true;
}

bool StoreManager::fncChange(std::string str)
{
	std::cout << "StoreManager::fncChange >> " << str << std::endl;
	try{
		std::vector<std::string> args;
		MyUtils::split(str, umt::WHITESPACES, args);

		if (args.size() < 2)
			return false;

		std::string sql_cmd = "UPDATE umtdb.store SET store_string = ? where user_id = ? AND store_id = ?;";
		sql::PreparedStatement *prep_stmt = m_con->prepareStatement(sql_cmd);
		prep_stmt->setString(1, args[1]);
		prep_stmt->setInt(2, m_userid);
		prep_stmt->setInt(3, boost::lexical_cast<int>(args[0]));
		prep_stmt->executeUpdate();
		delete prep_stmt;
	}
	catch (...) {
		return false;
	}
	return true;
}


} /* namespace umt */
