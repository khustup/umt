/*
 * UMTEngine.cpp
 *
 *  Created on: Jul 2, 2015
 *      Author: gevorg
 */

#include "UMTEngine.h"
#include "StoreManager.h"
#include "MyUtils.h"

#include <iostream>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <memory>

//#define WHITESPACES " \t-:"

namespace umt {

const std::string WHITESPACES = " \t-:";

UMTEngine::UMTEngine() : m_driver(NULL), m_con(NULL), m_storeMgr(NULL), m_info(NULL)
{
	// TODO Auto-generated constructor stub

}

UMTEngine::~UMTEngine() {
	// TODO Auto-generated destructor stub
}

bool UMTEngine::init(const std::string& strHost, const std::string& strUser, const std::string& strPass)
{
	try
	{
		  /* Create a connection */
		  m_driver = get_driver_instance();
		  m_con = m_driver->connect(strHost, strUser, strPass);
	}
	catch (sql::SQLException &e)
	{
		  std::cout << "# ERR: " << e.what();
		  std::cout << " (MySQL error code: " << e.getErrorCode();
		  std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
		  return false;
	}
	m_mapCommands["login"] = &UMTEngine::fncCmdLogin;
	return true;
}

bool UMTEngine::CreateUMTDB()
{
	sql::Statement *stmt;
	try{
		stmt = m_con->createStatement();
		stmt->execute("CREATE DATABASE IF NOT EXISTS umtdb; USE umtdb;");

		stmt->execute("CREATE TABLE IF NOT EXISTS users(id INT AUTO_INCREMENT PRIMARY KEY, "
			  	  	  	  	  	  	  	  	  	  	  "username VARCHAR(40) not null,"
			  	  	  	  	  	  	  	  	  	  	  "pwdhash TEXT(256) not null, "
			  	  	  	  	  	  	  	  	  	  	  "privileges ENUM('user','admin') not null Default 'user', "
			  	  	  	  	  	  	  	  	  	  	  "UNIQUE KEY(username)"
			  	  	  	  	  	  	  	  	  	  	  ";)");

		stmt->execute("CREATE TABLE IF NOT EXISTS store (user_id INT not null, store_id INT not null, store_string VARCHAR(200) not null"
			  	  	  	  	  	  	  	  	  	  	  ", UNIQUE KEY (user_id, store_id)"
			  	  	  	  	  	  	  	  	  	  	  ", INDEX (user_id)"
			  	  	  	  	  	  	  	  	  	  	  ", FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE"
			  	  	  	  	  	  	  	  	  	  	  ");");
	}
	catch (...)
	{
		delete stmt;
		std::cout << "Error on creating 'umtdb'" << std::endl;
		return false;
	}
	delete stmt;
	return true;
}

bool UMTEngine::processCommand(const std::string& strCmd, const std::string& strArgument)
{
	std::map<std::string, TypeCommandFnc>::const_iterator iter = m_mapCommands.find(strCmd);
	if (iter == m_mapCommands.end())
	{
		std::cout << "Insufficient privileges or unknown command: " << strCmd << std::endl;
		return false;
	}

	TypeCommandFnc fnc = m_mapCommands[strCmd];
	return (this->*fnc)(strArgument);
}


bool UMTEngine::fncCmdLogin(std::string strArgument)
{
	std::cout << "UMTEngine::fncCmdLogin >> " << strArgument << std::endl;
	boost::algorithm::trim(strArgument);
	std::size_t pos = strArgument.find_first_of(" \t-:");
	if (pos == std::string::npos)
	{
		std::cout<<"User/Password error"<<std::endl;
		return false;
	}
	std::string user = strArgument.substr(0, pos);
	std::string password = strArgument.substr(pos+1);
	boost::algorithm::trim(password);

	sql::Statement *stmt = m_con->createStatement();
	std::string sel_cmd = "SELECT id, privileges FROM umtdb.users WHERE username = '" + user + "' AND pwdhash = md5('" + password + "');";
	sql::ResultSet* res = stmt->executeQuery(sel_cmd);
	if (res->next())
	{ // Logging in
		m_storeMgr = new StoreManager(m_con, res->getInt(1));

		m_info = new user_info;
		m_info->uid = res->getInt(1);
		m_info->name = user;
		m_info->user_type = (res->getString(2) == "admin")? ADMIN : USER;

		// Adding function pointers
		m_mapCommands["add"] 	= &UMTEngine::store_add;
		m_mapCommands["remove"] = &UMTEngine::store_remove;
		m_mapCommands["list"] 	= &UMTEngine::store_list;
		m_mapCommands["change"] = &UMTEngine::store_change;

		//if (res->getString(2) == "admin")
		if (m_info->user_type == ADMIN)
		{
			m_mapCommands["register"] 	 = &UMTEngine::fncCmdRegister;
			m_mapCommands["remove_user"] = &UMTEngine::fncCmdRemove_user;
		}

		m_mapCommands["logout"]	= &UMTEngine::fncCmdLogout;

		// Disable "login" command
		auto it = m_mapCommands.find("login");
		//std::map<std::string, TypeCommandFnc>::iterator it = m_mapCommands.find("login");
		m_mapCommands.erase(it);
	}
	delete res;
	delete stmt;

	return true;
}

bool UMTEngine::fncCmdLogout(std::string strArgument)
{
	m_mapCommands.clear();

	delete m_storeMgr;
	m_storeMgr = NULL;

	m_mapCommands["login"] = &UMTEngine::fncCmdLogin;
	return true;
}


bool UMTEngine::fncCmdRegister(std::string strArgument)
{
	std::cout << "UMTEngine::fncCmdRegister >> " << strArgument << std::endl;
	//;
	try{
		std::vector<std::string> args;
		MyUtils::split(strArgument, std::string(WHITESPACES), args);

		if (args.size() < 3)
			return false;

		std::string sql_cmd = "INSERT INTO umtdb.users(username, pwdhash, privileges) VALUES (?, md5(?), ?) ;";
		std::unique_ptr<sql::PreparedStatement> prep_stmt{m_con->prepareStatement(sql_cmd)};
		prep_stmt->setString(1, args[0]);
		prep_stmt->setString(2, args[1]);
		prep_stmt->setString(3, args[2]);
		prep_stmt->execute();
	}
	catch (...) {
		return false;
	}
	return true;
}

bool UMTEngine::fncCmdRemove_user(std::string strArgument)
{
	std::cout << "UMTEngine::fncCmdRemove_user >> " << strArgument << std::endl;
	try{
		std::vector<std::string> args;
		MyUtils::split(strArgument, std::string(WHITESPACES), args);

		if (args.size() < 1)
			return false;
		std::string sql_cmd = "DELETE FROM umtdb.users WHERE username = ?;";
		sql::PreparedStatement *prep_stmt = m_con->prepareStatement(sql_cmd);
		prep_stmt->setString(1, args[0]);
		prep_stmt->execute();
	}
	catch (...) {
		return false;
	}

	return true;
}

// wrappers
bool UMTEngine::store_add(std::string strArgument)
{
	return m_storeMgr->fncAdd(strArgument);
}

bool UMTEngine::store_remove(std::string strArgument)
{
	return m_storeMgr->fncRemove(strArgument);
}

bool UMTEngine::store_list(std::string strArgument)
{
	return m_storeMgr->fncList(strArgument);
}

bool UMTEngine::store_change(std::string strArgument)
{
	return m_storeMgr->fncChange(strArgument);
}

} /* namespace umt */
