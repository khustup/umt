// The entry-point file

#include <stdlib.h>
#include <iostream>
#include <string>
#include <boost/algorithm/string.hpp>

#include "UMTEngine.h"

void print_usage()
{
	std::cout << "You have to provide MySQL's host, username & password." << std::endl;
}

int main (int argc, char** argv)
{
	/*
	if(argc < 3)
	{
		print_usage();
		return EXIT_FAILURE;
	}
	*/
	const char* const STOPCOMMAND = "stop";
	std::string inputStr;
	umt::UMTEngine umtObj;

	//if (!umtObj.init(argv[0], argv[1], argv[2]))
	if (!umtObj.init("tcp://127.0.0.1:3306", "root", "root"))
		return EXIT_FAILURE;
	while (true)
	{
		std::cout << "umt:>> ";
		std::getline(std::cin, inputStr);
		boost::algorithm::trim(inputStr);

		if (inputStr == STOPCOMMAND)
			break;

		try {
			std::size_t pos = inputStr.find_first_of(" \t-:");
			std::string command  = inputStr.substr(0, pos);
			std::string argument = (pos < inputStr.size())? inputStr.substr(pos) : "";
			if ( umtObj.processCommand(command, argument) )
				std::cout << "Success!" << std::endl;
			else
				std::cout << "Not to good..." << std::endl;
		}
		catch (...) {
			std::cout << "Incomplete command! Try again." << std::endl;
		}
		//sleep(1000);
	}
	return EXIT_SUCCESS;
}
