/*
 * UMTEngine.h
 *
 *  Created on: Jul 2, 2015
 *      Author: gevorg
 */

#ifndef UMTENGINE_H_
#define UMTENGINE_H_

#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

#include <map>

namespace umt {

class StoreManager; // predefinition
struct user_info;

class UMTEngine {

	typedef bool (UMTEngine::*TypeCommandFnc)(std::string );

public:
	UMTEngine();
	virtual ~UMTEngine();

	bool init(const std::string& strHost, const std::string& strUser, const std::string& strPass);
	bool processCommand(const std::string& strCmd, const std::string& strArgument);

private:

	// Command functions
	bool fncCmdLogin(std::string strArgument);
	bool fncCmdLogout(std::string strArgument);
	bool fncCmdRegister(std::string strArgument);
	bool fncCmdRemove_user(std::string strArgument);
	bool store_add(std::string strArgument);
	bool store_remove(std::string strArgument);
	bool store_list(std::string strArgument);
	bool store_change(std::string strArgument);

	bool CreateUMTDB();


private: //Private mambers
	  sql::Driver*		m_driver;
	  sql::Connection*	m_con;
	  StoreManager*		m_storeMgr;
	  std::map<std::string, TypeCommandFnc> m_mapCommands;
	  user_info*		m_info;
};

enum user_types {UNKNOWN, USER, ADMIN};

struct user_info {
	int	uid;
	std::string name;
	user_types	user_type;
};


} /* namespace umt */

#endif /* UMTENGINE_H_ */
