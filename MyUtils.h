/*
 * MyUtils.h
 *
 *  Created on: Jul 8, 2015
 *      Author: gevorg
 */

#ifndef MYUTILS_H_
#define MYUTILS_H_

#include <string>
#include <vector>
#include <sstream>

namespace umt {

class MyUtils {
public:
	static void split(const std::string& str, const std::string& delimiters, std::vector<std::string>& tokens);

	template<typename T>
	static std::string to_string(const T& value)
	{
	    std::ostringstream oss;
	    oss << value;
	    return oss.str();
	};

};

} /* namespace umt */

#endif /* MYUTILS_H_ */
