/*
 * StoreManager.h
 *
 *  Created on: Jul 5, 2015
 *      Author: gevorg
 */

#ifndef STOREMANAGER_H_
#define STOREMANAGER_H_

#include <string>

#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>


namespace umt {

class StoreManager {

public:
	StoreManager(sql::Connection* con, int user_id);
	virtual ~StoreManager();

	bool fncAdd(std::string str);
	bool fncRemove(std::string str);
	bool fncList(std::string str);
	bool fncChange(std::string str);

private:
	int m_userid;
	sql::Connection*	m_con;
};

} /* namespace umt */

#endif /* STOREMANAGER_H_ */
